
/* NOTES

TODO:
Create Ladder Logic (LD) diagram to control robot arm, use pin mapping below.
Create Scada plots.
Create ROS node that uses PyModbus as master.
Make robot arm Modbus slave to receive commands from ROS, look up Modbus pin mapping.

Pin mapping
https://www.openplcproject.com/runtime/arduino/

Robot arm
https://wiki.keyestudio.com/KS0488_Keyestudio_4DOF_Robot_Arm_DIY_Kit_V2.0_for_Arduino#Project_2:_Joint_Rotation_and_Pin_Control

OpenPLC dashboard
http://localhost:8080/dashboard
openplc:openplc

ScadaBR
sudo /opt/tomcat6/apache-tomcat-6.0.53/bin/startup.sh
http://localhost:9090/ScadaBR/
admin:admin

OpenPLC tutorial
https://www.openplcproject.com/reference/basics/first-project.html

Scada Manual
https://sourceforge.net/p/scadabr/wiki/Manual%20ScadaBR%20English%200%20Summary/

Example deployments:
RaspberryPi (OpenPLC runtime) w/ motor hat -> Arm servos (Minimal)
PC (ROS/2) -> RaspberryPi (OpenPLC runtime) w/ motor hat -> Arm servos
PC (ROS/2) -> RaspberryPi (OpenPLC runtime) -> Arduino w/ motor shield -> Arm servos (Best, fewest performance issues)

Comment: 
Minimal deployment would have only Scada as interface, vision/ML compute on RPi CPU would compromise real-time performance.
Best (3rd) solution is most deterministic and capale of semantic tracking.
But minimal version would still be desireable for simple IoT deployments where vision/ML compute is not needed or can be offloaded to Coral TPU.
A direct PC -> Arduino solution is possible with micro-ROS, but the control/logic runtime must be hand coded and flashed to Arduino.
Hand coding is slower, and the Raspberry Pi is a better microcontroller than the Arduino because it is faster and has more connectivity and expansion options.
Also, the newer microcontrollers like the Arduino RP2040 Zero already run the RPi CPU.
Can Modbus communicate poses? If yes then the vision system on the PC can generate the poses and send them to the OpenPLC runtime, and the PIDs in the PLC runtime can update accordingly.
The answer seems to be yes. The Modbus master can write to registers in the "outstation".

Ideal/quick solution:
PC (ROS2) -> (Modbus network) -> RPi (OpenPLC) -> (OpenPLC slave) -> Arduino w/ motor shield -> Robot servos

Problem:
Where does micro-ROS fit in? You would use it where you needed to hand code the control logic on the microcontroller.
You would need to do this if you had hardware that needed to be part of the control loop that PLC did not have access to.
And once the control logic becomes complex then code will be better to manage than diagrams.
But if you are coding directly to the microcontroller then you don't need the PLC runtime.
This is the purpose of micro-ROS, more complex algorithms with fewer components.
On the other hand, if you have lots of motors, then coding each microcontroller becomes combersome.
Using lots of Arduinos as slaves to a central RPi running the control logic allows larger scale.
In the long run, coding each microcontroller with micro-ROS gives better control and reliability, but it seems that projects with limited resources would do better with PLCs.
The other issue is that the RPi microcontroller doesn't take motor hats, meaning that for quick projects you would need the an Arduino Zero or an RTOS board.
Another problem with running micro-ROS is that it requires more coordination between controllers, each controller has to sync its motors with other controllers.
The PLC hub/spoke solution requires no coordinatio between controllers, only within its own runtime.

Ideal micro-ROS solution:
PC (ROS2) -> Arduino Zero w/ motor shield (micro-ROS) -> Robot servos

*/


// Keyestudio Pin Control
//
// Name 	                    IO Pin
// Servo 1 (baseplate) 	        A1
// Servo 2 (left side) 	        A0
// Servo 3（right side）         8
// Servo 4（clamp claw） 	       9
// Right Joystick X             A2
// Right Joystick Y 	          A5
// Right Joystick Z (key) 	    7
// Left Joystick X 	            A3
// Left Joystick Y 	            A4
// Left Joystick Z 	            6
// D1/DAT of PS2 	              12
// D0/CMD of PS2 	              10
// CE/SEL of PS2 	              11
// CLK of PS2 	                13 


// OpenPLC Pin Mapping
//
// Below you can find the pin mapping for Arduino Uno and Arduino Mega boards. 
// The table was built for OpenPLC v2, but it is still valid for OpenPLC v3, 
// except that the slave address on v3 starts at position 100 
// (e.g. %IX100.0, %IX100.1, etc).
//
// Digital Inputs
//          Arduino Pin         OpenPLC I/O
//          2                   %IX0.0
//          3                   %IX0.1
//          4                   %IX0.2
//          5                   %IX0.3
//          6                   %IX0.4
// Digital Outputs
//          7                   %QX0.0
//          8                   %QX0.1
//          12                  %QX0.2
//          13                  %QX0.3
// Analog Inputs
//          A0                  %IW0.0
//          A1                  %IW0.1
//          A2                  %IW0.2
//          A3                  %IW0.3
//          A4                  %IW0.4
//          A5                  %IW0.5
// Analog Outputs
//          9                   %QW0.0
//          10                  %QW0.1
//          11                  %QW0.2


// OpenPLC arm controls:
// Servo 1 (baseplate) 	        %IW0.1
// Servo 2 (left side) 	        %IW0.0
// Servo 3（right side）         %QX0.1
// Servo 4（clamp claw） 	       %QW0.0
// Right Joystick X             %IW0.2
// Right Joystick Y 	          %IW0.5
// Right Joystick Z (key) 	    %QX0.0
// Left Joystick X 	            %IW0.3
// Left Joystick Y 	            %IW0.4
// Left Joystick Z 	            %IX0.4
// D1/DAT of PS2 	              %QX0.2
// D0/CMD of PS2 	              %QW0.1
// CE/SEL of PS2 	              %QW0.2
// CLK of PS2 	                %QX0.3



// Basic joystick version
#include <Servo.h>  //add Servo library file
Servo myservo1;  // create servo object to control a servo
Servo myservo2;
Servo myservo3;
Servo myservo4;  

int pos1=90, pos2=60, pos3=130, pos4=0;  // define the variable of 4 servo angle,and assign the initial value (that is the boot posture angle value) 


const int right_X = A2; // define the right X pin to A2
const int right_Y = A5; //define the right Y pin to A5
const int right_key = 7; // define the right key to 7(the value of Z axis)
const int left_X = A3; // define the left X pin to A3
const int left_Y = A4;  // define the left Y pin to A4
const int left_key = 6; // define the left key to 6(the value of Z axis)

int x1,y1,z1;  //define a variable to store the read Joystick value
int x2,y2,z2;

void setup() 
{
  //Serial.begin(9600); //  set the baud rate to 9600 

  //start up posture 
  myservo1.write(pos1);  
  delay(1000);
  myservo2.write(pos2);
  myservo3.write(pos3);
  myservo4.write(pos4);
  delay(1500);

  pinMode(right_key, INPUT);   // set the right/left key to INPUT
  pinMode(left_key, INPUT);
  //Serial.begin(9600); //  set baud rate to 9600
}
 
void loop() 
{
  myservo1.attach(A1);  //set the control pin of Servo1 to A1
  myservo2.attach(A0);  //set the control pin of Servo2 to A0
  myservo3.attach(8);   //set the control pin of Servo3 to D8
  myservo4.attach(9);   //set the control pin of Servo4 to D9

  x1 = analogRead(right_X); // read the value of right X axis

  y1 = analogRead(right_Y);  // read the value of right Y axis
  z1 = digitalRead(right_key);  ////read the value of right Z axis
  
  x2 = analogRead(left_X);  //read the value of left X axis
  y2 = analogRead(left_Y);  //read the value of left Y axis
  z2 = digitalRead(left_key);  //read the value of left Z axis
  //delay(5);  //lower the speed overall
    //Serial.println("**********right**********");
  //Serial.print("right_X = "); // on the serial monitor, print out right_X = 
  //Serial.print(x1 ,DEC); // print out the value of right X and line wrap
  //Serial.print("  ");
  //Serial.print("right_Y = ");
  //Serial.print(y1 ,DEC);
  //Serial.print("  ");
  //Serial.print("right_key = ");
  //Serial.print(z1 ,DEC);
  //Serial.print(" ||| ");
  //Serial.println("*********left***********");
  //Serial.print("left_X = ");
  //Serial.print(x2 ,DEC);
  //Serial.print("  ");
  //Serial.print("left_Y = ");
  //Serial.print(y2 ,DEC);
  //Serial.print("  ");
  //Serial.print("left_key = ");
  //Serial.println(z2 ,DEC);

  //clamp claw
  zhuazi();
  //rotate
  zhuandong();
  //Right Servo
  right_ser();
  //Left Servo
  left_ser();
}

//Claw
void zhuazi()
{
    //Claw
  if(x2<50) //if push the left joystick to the right
  {
      pos4=pos4-1;  //current angle of servo 4 subtracts 1（change the value you subtract, thus change the closed speed of claw）
      //Serial.println(pos4);
      myservo4.write(pos4);  //Servo4 operates the action，claw gradually closes
      delay(5);
      if(pos4<0)  //if pos4 value subtracts to 0
      {            //（change value according to real situation）
        pos4=0;   //stop subtraction when reduce to 0
      }
   }
  if(x2>1000) ////if push the left joystick to the left 
  {
      pos4=pos4+2; // current angle of servo 4 plus 2（change the value you plus, thus change the open speed of claw）

      //Serial.println(pos4);
      myservo4.write(pos4); //Servo4 operates the motion, the claw gradually opens.

      delay(5);
      if(pos4>110)  //limit the largest angle when open the claw 
      {
        pos4=110;                                
      }
  }
}
//******************************************************
 //rotate
void zhuandong()
{
  if(x1<50)  // if push the right joystick to the right
  {
    pos1=pos1-1;  //pos1 subtracts 1
    myservo1.write(pos1);  //Servo1 operates the motion, the arm turns right.
    delay(5);
    if(pos1<1)   //limit the angle when turn right
    {
      pos1=1;
    }
  }
  if(x1>1000)  // if push the right joystick to the left
  {
    pos1=pos1+1;  //pos1 plus 1
    myservo1.write(pos1);  //arm turns left 
    delay(5);
    if(pos1>180)  //limit the angle when turn left 
    {
      pos1=180;
    }
  }
}

//**********************************************************/
//Right Servo
void right_ser()
{
    if(y1<50) //if push the right joystick backward
  {
    pos3=pos3-1;
    myservo3.write(pos3); //the joystick of right Servo swings backward
    delay(5);
    if(pos3<0)  //limit the angle
    {
      pos3=0;
    }
  }
  if(y1>1000)  // if push the right joystick forward
  {
    pos3=pos3+1;  
    myservo3.write(pos3);  //the joystick of right Servo swings forward
    delay(5);
    if(pos3>180)  //limit the angle when go down
    {
      pos3=180;
    }
  }
}

//*************************************************************/
//Left Servo
void left_ser()
{
  if(y2<50)  //if push the left joystick backward 
  {
    pos2=pos2+1;
    myservo2.write(pos2);  //the joystick of left Servo swings backward
    delay(5);
    if(pos2>180)   // limit the retracted angle 
    {
      pos2=180;
    }
  }
  
  if(y2>1000)  //if push the left joystick forward 
  {
    pos2=pos2-1;
    myservo2.write(pos2);  //the joystick of left Servo swings forward
    delay(5);
    if(pos2<35)  // Limit the the stretched angle
    {
      pos2=35;
    }
  }

}  
